#pragma once
#include "mapas.h"
#include "movimiento_personajes.h"
#include "cinematica.h"
#include "dragon_boss.h"
bool e_movimiento = true;
bool e_mapa = false;
bool boss_final = false;
extern int zerox, zeroy;
void cambiar_mapa();
void zeromov(int& x, int& y, int mapeo[30][120]);

void cambiar_estado() {
	for (;;) {
		if (e_mapa) {
			system("cls");
			cambiar_mapa();
			e_mapa = false;
			e_movimiento = true;
		}
		if (e_movimiento) {
			zeromov(zerox, zeroy, mapeo);
		}
		if (boss_final) {
			system("cls");
			bienvenidaDragon();
			juego_boss();
			despedidaAnciano();
			break;
		}
	}
}