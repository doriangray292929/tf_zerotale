#pragma once

using namespace std;
using namespace System;

#define VE (char)179
#define TR (char)191
#define BL (char)192
#define HO (char)196
#define BR (char)217
#define TL (char)218

void Imprimirxy(int x, int y, char txt) {

    Console::SetCursorPosition(x, y);
    cout << txt;

}

void marco_cuadrado(int row, int col, int width, int height) {
    gotoxy(col, row);
    cout << TL;
    gotoxy(col + width - 1, row);
    cout << TR;
    gotoxy(col, row + height - 1);
    cout << BL;
    gotoxy(col + width - 1, row + height - 1);
    cout << BR;
    for (int i = 1; i < height - 1; ++i) {
        gotoxy(col, row + i);
        cout << VE;
        gotoxy(col + width - 1, row + i);
        cout << VE;
    }
    for (int i = 1; i < width - 1; ++i) {
        gotoxy(col + i, row);
        cout << HO;
        gotoxy(col + i, row + height - 1);
        cout << HO;
    }
}

void marco_dialogo(int row, int col, int width, int height) {
    gotoxy(col, row);
    cout << '*';
    gotoxy(col + width - 1, row);
    cout << '*';
    gotoxy(col, row + height - 1);
    cout << '*';
    gotoxy(col + width - 1, row + height - 1);
    cout << '*';
    for (int i = 1; i < height - 1; ++i) {
        gotoxy(col, row + i);
        cout << '|';
        gotoxy(col + width - 1, row + i);
        cout << '|';
    }
    for (int i = 1; i < width - 1; ++i) {
        gotoxy(col + i, row);
        cout << '-';
        gotoxy(col + i, row + height - 1);
        cout << '-';
    }
}

void verificar_res(int pre, int res) {
    int x = 40;
    int y = 10;
    system("cls");
    if (pre == res) {
        color(BRIGHT_GREEN);
        gotoxy(x, y);
        cout << "   ____             U _____ u _   _     " << endl;
        gotoxy(x, y + 1);
        cout << "U | __\")u    ___    \\| ___\"|/| \\ |\"|    " << endl;
        gotoxy(x, y + 2);
        cout << " \\|  _ \\/   |\"_|     |  _|\" <|  \\| |>   " << endl;
        gotoxy(x, y + 3);
        cout << "  | |_) |    | |     | |___ U| |\\  |u  " << endl;
        gotoxy(x, y + 4);
        cout << "  |____/   U/| |\\u   |_____| |_| \\_|   " << endl;
        gotoxy(x, y + 5);
        cout << " _|| \\\\_.-,_|___|_,-.<<   >> ||   \\\\,-." << endl;
        gotoxy(x, y + 6);
        cout << "(__) (__)\_)-' '-(_/(__) (__)(_)\")  (_/  " << endl;
        clearColor();
    }
    else {
        x += 8;
        color(BRIGHT_RED);
        gotoxy(x, y);
        cout << "  __  __      _       _      " << endl;
        gotoxy(x, y + 1);
        cout << "U|' \\/ '|uU  /\"\\  u  |\"|     " << endl;
        gotoxy(x, y + 2);
        cout << "\\| |\\/| |/ \\/ _ \\/ U | | u   " << endl;
        gotoxy(x, y + 3);
        cout << " | |  | |  / ___ \\  \\| |/__  " << endl;
        gotoxy(x, y + 4);
        cout << " |_|  |_| /_/   \\_\\  |_____| " << endl;
        gotoxy(x, y + 5);
        cout << "<<,-,,-.   \\\\    >>  //  \\\\  " << endl;
        gotoxy(x, y + 6);
        cout << " (./  \\.) (__)  (__)(_)\")(\"_) " << endl;
        clearColor();
    }
}

void marco_dialogo2(int x, int y, int ancho, int largo, string txt) {

    const char EIU = '+';
    const char EDU = '+';
    const char EIA = '+';
    const char EDA = '+';
    const char BORDEAU = '-';
    const char LATERAL = '|';
    int INICIO_UP = x; // Para la parte arriba y abajo del marco
    int INICIO_COST = y; // Para los costados del marco

    Imprimirxy(INICIO_UP, INICIO_COST - 1, EIU);
    Imprimirxy(ancho + INICIO_UP, INICIO_COST - 1, EDU);
    Imprimirxy(INICIO_UP, largo + INICIO_COST - 2, EIA);
    Imprimirxy(ancho + INICIO_UP, largo + INICIO_COST - 2, EDA);

    for (int i = INICIO_UP + 1; i < ancho + INICIO_UP; ++i) {
        Imprimirxy(i, INICIO_COST - 1, BORDEAU);
        Imprimirxy(i, INICIO_COST + largo - 2, BORDEAU);
    }
    for (int i = INICIO_COST + 1; i < largo + INICIO_COST - 1; ++i) {
        Imprimirxy(INICIO_UP, i - 1, LATERAL);
        Imprimirxy(INICIO_UP + ancho, i - 1, LATERAL);
    }

    Console::SetCursorPosition(x + 2, y + 1);
    cout << txt;
}
void limpiarPantallaZ() {
    gotoxy(48, 27);
    cout << "Presiona z tecla para continuar...";
    while (true) {
        if (_kbhit()) {
            char tecla = tolower(getch());
            if (tecla == 'z') {
                system("cls");
                break;
            }
        }
    }
}

void lineah(int a, int b, int c, char txt) {
    b++;
    for (int i = 0; i < a; ++i) {
        mapeo[b][c + i] = 1;
        gotoxy(c + i, b);
        cout << txt;
    }
}

void lineav(int a, int b, int c, char txt) {
    b++;
    for (int i = 0; i <= a; ++i) {
        for (int j = 0; j < i; ++j) {
            mapeo[b + i][c] = 1;
            gotoxy(c, b + i);
            cout << txt;
        }

    }
}

void borrar_lineah(int a, int b, int c, char txt) {
    b++;
    for (int i = 0; i < a; ++i) {
        mapeo[b][c + i] = 1;
        gotoxy(c + i, b);
        cout << txt;
    }
}
void coloresOpciones(int colorn) {

    switch (colorn) {

    case 1:
        color(DARK_GREEN);
        break;
    case 2:
        color(BRIGHT_RED);
        break;
    case 3:
        color(DARK_MAGENTA);
        break;
    case 4:
        color(BRIGHT_CYAN);
        break;
    }
}
void opciones(string opcion1, string opcion2, string opcion3, int colorn) {

    coloresOpciones(colorn);
    marco_dialogo2(9, 20, 17, 5, opcion1);
    clearColor();
    marco_dialogo2(36, 20, 17, 5, opcion2);
    marco_dialogo2(63, 20, 17, 5, opcion3);
}
bool elegir_opcion = true;
bool respuestaIncorrecta = false;

void elegir(char opcionCorrecta, string opcion1, string opcion2, string opcion3, int colorn) {

    int xopcion = 15;
    int yopcion = 17;

    char opcionActual = '1';  // Opcin inicial

    // Pocisin de las opciones X

    int opciones[3] = { 17, 45, 72 };

    // Dibujar 'v' en la posicin inicial
    gotoxy(opciones[0], yopcion + 1);
    cout << 'v';

    while (elegir_opcion) {
        if (_kbhit()) {  // Verificar si se ha presionado una tecla
            char tecla = _getch();
            respuestaIncorrecta = false;


            // Limpiar 'v' en la posicin actual
            gotoxy(opciones[opcionActual - '1'], yopcion + 1);
            cout << " ";

            gotoxy(opciones[opcionActual - '1'], yopcion);

            coloresOpciones(colorn);
            marco_dialogo2(9, 20, 17, 5, opcion1);
            clearColor();
            marco_dialogo2(36, 20, 17, 5, opcion2);
            marco_dialogo2(63, 20, 17, 5, opcion3);

            if (tecla == 'l') {  // Mover a la derecha
                if (opcionActual < '3') {
                    opcionActual++;
                }
            }
            else if (tecla == 'j') {  // Mover a la izquierda
                if (opcionActual > '1') {
                    opcionActual--;
                }
            }
            else if (tecla == 'z') {  // Salir del bucle
                if (opcionActual == opcionCorrecta) {
                    verificar_res(2, 2);
                    elegir_opcion = false;
                    limpiarPantallaZ();
                    break;
                }
                else {
                    verificar_res(2, 1);

                    elegir_opcion = false;
                    respuestaIncorrecta = true;
                    limpiarPantallaZ();
                    break;
                }
            }

            if (opcionActual == '1') coloresOpciones(colorn);  marco_dialogo2(9, 20, 17, 5, opcion1); clearColor();
            if (opcionActual == '2') coloresOpciones(colorn); marco_dialogo2(36, 20, 17, 5, opcion2); clearColor();
            if (opcionActual == '3') coloresOpciones(colorn);; marco_dialogo2(63, 20, 17, 5, opcion3); clearColor();

            gotoxy(opciones[opcionActual - '1'], yopcion + 1);
            cout << 'v';

        }
    }

}
void dibujarManzanza_peque�a(int x, int y) {
    int manzana_peque�a[6][8] = { 0, 0, 0, 0, 0, 2, 0, 0,
                            0, 0, 0, 0, 2, 3, 0, 0,
                            0, 1, 1, 2, 3, 1, 1, 0,
                            1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1,
                            0, 1, 1, 1, 1, 1, 1, 0 };

    char n219 = 219;
    char n178 = 178;
    char n177 = 177;

    for (int i = 0; i < 6; ++i) {

        for (int j = 0; j < 8; ++j) {

            if (manzana_peque�a[i][j] == 1) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (manzana_peque�a[i][j] == 2) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << n178;
                clearColor();

            }
            if (manzana_peque�a[i][j] == 3) {

                color(DARK_GREEN);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }

        }

    }


}
void dibujarManzanza_peque�ita(int x, int y) {
    int manzana_peque�ita[4][5] = { 0, 0, 2, 3, 0,
                                    0, 1, 2, 1, 0,
                                    1, 1, 1, 1, 1,
                                    0, 1, 1, 1, 0 };

    char n219 = 219;
    char n178 = 178;
    char n177 = 177;

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 5; ++j) {

            if (manzana_peque�ita[i][j] == 1) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (manzana_peque�ita[i][j] == 2) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << n178;
                clearColor();

            }
            if (manzana_peque�ita[i][j] == 3) {

                color(DARK_GREEN);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }

        }

    }


}
void borrarManzanza_peque�ita(int x, int y) {
    int manzana_peque�ita[4][5] = { 0, 0, 2, 3, 0,
                                    0, 1, 2, 1, 0,
                                    1, 1, 1, 1, 1,
                                    0, 1, 1, 1, 0 };

    char n219 = 219;
    char n178 = 178;
    char n177 = 177;

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 5; ++j) {

            if (manzana_peque�ita[i][j] == 1) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << ' ';
                clearColor();

            }
            if (manzana_peque�ita[i][j] == 2) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << ' ';
                clearColor();

            }
            if (manzana_peque�ita[i][j] == 3) {

                color(DARK_GREEN);
                gotoxy(x + j, y + i);
                cout << ' ';
                clearColor();

            }

        }

    }


}
void dibujarBolsa(int x, int y) {
    int bolsa[6][6] = { 0, 0, 1, 1, 1, 1,
                        0, 2, 1, 1, 2, 0,
                        1, 1, 2, 2, 1, 1,
                        1, 1, 1, 1, 1, 1,
                        1, 1, 1, 1, 1, 1,
                        0, 1, 1, 1, 1, 0 };

    char n219 = 219;
    char n178 = 178;
    char n177 = 177;

    for (int i = 0; i < 6; ++i) {

        for (int j = 0; j < 6; ++j) {

            if (bolsa[i][j] == 1) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << n177;
                clearColor();

            }
            if (bolsa[i][j] == 2) {

                color(DARK_YELLOW);
                gotoxy(x + j, y + i);
                cout << n178;
                clearColor();

            }

        }

    }


}
void dibujarEstrella(int x, int y) {
    int estrella[4][5] = { 0, 0, 1, 0, 0,
                        1, 1, 1, 1, 1,
                        0, 1, 1, 1, 0,
                        1, 0, 0, 0, 1 };

    char n219 = 219;
    char n178 = 178;
    char n177 = 177;

    for (int i = 0; i < 4; ++i) {

        for (int j = 0; j < 5; ++j) {

            if (estrella[i][j] == 1) {

                color(DARK_YELLOW);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }


        }

    }


}
void borrarManzanza_peque�a(int x, int y) {
    int manzana_peque�a[6][8] = { 0, 0, 0, 0, 0, 2, 0, 0,
                            0, 0, 0, 0, 2, 3, 0, 0,
                            0, 1, 1, 2, 3, 1, 1, 0,
                            1, 1, 1, 1, 1, 1, 1, 1,
                            1, 1, 1, 1, 1, 1, 1, 1,
                            0, 1, 1, 1, 1, 1, 1, 0 };

    char n219 = 219;
    char n178 = 178;
    char n177 = 177;

    for (int i = 0; i < 6; ++i) {

        for (int j = 0; j < 8; ++j) {

            if (manzana_peque�a[i][j] == 1) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << ' ';
                clearColor();

            }
            if (manzana_peque�a[i][j] == 2) {

                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << ' ';
                clearColor();

            }
            if (manzana_peque�a[i][j] == 3) {

                color(DARK_GREEN);
                gotoxy(x + j, y + i);
                cout << ' ';
                clearColor();

            }

        }

    }


}
void dibujarCorazon(int x, int y) {
    int cora[5][5] = { 1, 1, 0, 1, 1,
                    1, 1, 1, 1, 1,
                    0, 1, 1, 1, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 0, 0 };

    char n219 = 219;

    for (int i = 0; i < 5; ++i) {

        for (int j = 0; j < 5; ++j) {

            if (cora[i][j] == 1) {
                color(BRIGHT_RED);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
        }

    }
}
void dibujarPinguino(int x, int y) {
    long long int pinguino_arreglo[8][12] = { 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 1, 2, 0, 2, 2, 0, 2, 1, 0, 0,
        0, 0, 1, 2, 2, 3, 3, 2, 2, 1, 0, 0,
        0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0,
        0, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 0,
        0, 0, 1, 1, 1, 2, 2, 1, 1, 1, 0, 0,
        0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 5, 5, 0, 0, 0, 0, 5, 5, 0, 0,


    };
    char n219 = 219;


    for (int i = 0; i < 8; ++i) {

        for (int j = 0; j < 12; ++j) {
            gotoxy(x + j, y + i);

            if (pinguino_arreglo[i][j] == 1) {

                color(BRIGHT_BLACK);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_arreglo[i][j] == 2) {

                color(WHITE);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_arreglo[i][j] == 3) {

                color(BRIGHT_YELLOW);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_arreglo[i][j] == 4) {

                color(DARK_CYAN);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_arreglo[i][j] == 5) {

                color(DARK_YELLOW);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }


        }

    }


}
void dibujarCaraPinguino(int x, int y) {
    long long int pinguino_caraarreglo[5][7] = { 3, 3, 3, 3, 3, 3, 3,
                                            1, 1, 3, 3, 3, 1, 1,
                                            1, 0, 1, 3, 1, 0, 1,
                                            1, 1, 1, 2, 2, 1, 1,
                                            4, 4, 4, 4, 4, 4, 4
    };
    char n219 = 219;


    for (int i = 0; i < 5; ++i) {

        for (int j = 0; j < 7; ++j) {
            gotoxy(x + j, y + i);

            if (pinguino_caraarreglo[i][j] == 3) {

                color(BRIGHT_BLACK);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_caraarreglo[i][j] == 1) {

                color(WHITE);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_caraarreglo[i][j] == 2) {

                color(DARK_RED);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (pinguino_caraarreglo[i][j] == 4) {

                color(DARK_CYAN);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
        }

    }


}

void dibujar_iglu(int x, int y) {
    long long int iglus[7][13] = { 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                                    0, 1, 1, 3, 3, 3, 3, 3, 3, 3, 1, 1, 0,
                                    1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1,
                                    1, 3, 3, 3, 1, 1, 1, 1, 1, 3, 3, 3, 1,
                                    1, 3, 3, 1, 1, 4, 4, 4, 1, 1, 3, 3, 1,
                                    1, 1, 1, 1, 4, 4, 4, 4, 4, 1, 1, 1, 1,
                                    1, 1, 1, 1, 4, 4, 4, 4, 4, 1, 1, 1, 1
    };
    char n219 = 219;
    char n177 = 177;

    for (int i = 0; i < 7; ++i) {

        for (int j = 0; j < 13; ++j) {
            gotoxy(x + j, y + i);

            if (iglus[i][j] == 3) {

                color(BRIGHT_BLACK);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n177;
                clearColor();

            }
            if (iglus[i][j] == 1) {

                color(WHITE);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (iglus[i][j] == 4) {

                color(DARK_RED);
                mapeo[y + j][x + i] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
        }

    }


}
void dragon_dibujo(int x, int y) {



    long long int dragun[21][45] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 1, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 2, 2, 0, 0, 0, 2, 2, 0, 0, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 2, 2, 2, 0, 0, 0, 0, 0, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 3, 2, 2, 2, 2, 2, 2, 3, 3, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1,
        3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        0, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 5, 5, 2, 5, 4, 4, 3, 3, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1,
        0, 0, 3, 3, 0, 0, 3, 3, 3, 3, 3, 3, 0, 0, 3, 3, 3, 3, 3, 0, 2, 2, 0, 0, 5, 2, 5, 5, 5, 4, 4, 4, 4, 3, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 2, 0, 0, 0, 2, 0, 0, 0, 5, 5, 5, 4, 4, 2, 4, 4, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 5, 5, 5, 5, 4, 4, 2, 2, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 2, 4, 4, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 2, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0


    };
    char n219 = 219;


    for (int i = 0; i < 21; ++i) {

        for (int j = 0; j < 45; ++j) {
            gotoxy(x + j, y + i);

            if (dragun[i][j] == 1) {

                color(DARK_GREEN);
                mapeo[y + i][x + j] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (dragun[i][j] == 2) {

                color(WHITE);
                mapeo[y + i][x + j] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (dragun[i][j] == 3) {

                color(DARK_YELLOW);
                mapeo[y + i][x + j] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (dragun[i][j] == 4) {

                color(DARK_RED);
                mapeo[y + i][x + j] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (dragun[i][j] == 5) {

                color(BRIGHT_RED);
                mapeo[y + i][x + j] = 1;
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }


        }

    }
}

int nrandom(int maximo, int minimo) {
    return minimo + rand() % (maximo + 1 - minimo);
}



