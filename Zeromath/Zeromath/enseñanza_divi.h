#pragma once
#include "upc.h"
#include "utilidades.h"
#include "mapas.h"

extern int mapeo[30][120];
using namespace std;
void dibujar_pinguino(int x, int y) {
	long long int pinguino_arreglo[8][12] = { 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0,
		0, 0, 1, 2, 0, 2, 2, 0, 2, 1, 0, 0,
		0, 0, 1, 2, 2, 3, 3, 2, 2, 1, 0, 0,
		0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0,
		0, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 0,
		0, 0, 1, 1, 1, 2, 2, 1, 1, 1, 0, 0,
		0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0,
		0, 0, 5, 5, 0, 0, 0, 0, 5, 5, 0, 0,


	};
	char n219 = 219;


	for (int i = 0; i < 8; ++i) {

		for (int j = 0; j < 12; ++j) {
			gotoxy(x + j, y + i);

			if (pinguino_arreglo[i][j] == 1) {

				color(BRIGHT_BLACK);
				mapeo[y + j][x + i] = 1;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}
			if (pinguino_arreglo[i][j] == 2) {

				color(WHITE);
				mapeo[y + j][x + i] = 1;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}
			if (pinguino_arreglo[i][j] == 3) {

				color(BRIGHT_YELLOW);
				mapeo[y + j][x + i] = 1;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}
			if (pinguino_arreglo[i][j] == 4) {

				color(DARK_CYAN);
				mapeo[y + j][x + i] = 1;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}
			if (pinguino_arreglo[i][j] == 5) {

				color(DARK_YELLOW);
				mapeo[y + j][x + i] = 1;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}


		}

	}


}

void dibujarcolumna_pinguino(int n, int x, int y) {

	for (int i = 0; i < n; i++) {
		if (i > 0 && i % 3 == 0) {
			x += 15;
			y -= 3 * 9;
		}
		dibujar_pinguino(x, y);
		y += 9;
	}

}


void dibujar_pez(int x, int y) {
	long long int pez_dibujo[4][11]{ 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0,
		0, 0, 6, 6, 6, 0, 0, 0, 2, 0, 6,
		0, 0, 6, 6, 6, 0, 0, 0, 0, 6, 6,
		6, 6, 0, 0, 6, 6, 6, 6, 6, 6, 0,

	};
	char n219 = 219;
	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 11; ++j) {
			gotoxy(x + j, y + i);

			if (pez_dibujo[i][j] == 2) {

				color(WHITE);
				mapeo[y + j][x + i] = 6;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}
			if (pez_dibujo[i][j] == 6) {

				color(BRIGHT_CYAN);
				mapeo[y + j][x + i] = 1;
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();

			}


		}

	}


}





void dibujarcolumna_pez(int n, int x, int y) {

	for (int i = 0; i < n; i++) {
		if (i > 0 && i % 4 == 0) {
			x += 12;
			y -= 4 * 5;
		}
		dibujar_pez(x, y);
		y += 5;
	}

	while (y < 30) {

		gotoxy(x + 3, y);
		y++;
	}
}

void dibujarcolumna_res(int n, int x, int y) {

	for (int i = 0; i < n; i++) {
		dibujar_pinguino(x, y);
		x += 14;
	}

}

void marco_res_pingui(int res) {
	system("cls");
	marco_dialogo(0, 20, 80, 5);
	gotoxy(45, 2);
	if (res == 1) {
		cout << "En total cada pinguino comera " << res << " pez!!";
	}
	else {
		cout << "En total cada pinguino comera " << res << " peces!!";
	}

	dibujarcolumna_res(res, 1, 10);
}



void ronda_division(int s1, int s2) {
	int res_p;
	int res = s2 / s1;
	marco_cuadrado(24, 50, 20, 5);
	marco_dialogo(0, 30, 64, 5);
	gotoxy(32, 2);
	cout << "Hay " << s2 << " peces y " << s1 << " pinguinos. Cuantos debe tener cada pinguino? ";
	dibujarcolumna_pez(s2, 1, 5);
	dibujarcolumna_pinguino(s1, 94, 3);
	gotoxy(25, 26);
	cout << "Escribe tu respuesta -->";
	gotoxy(59, 26);
	showCursor();
	cin >> res_p;
	hideCursor();

	verificar_res(res, res_p);
	sleep4(1000);
	marco_res_pingui(res);
	sleep4(1500);
	system("cls");
}

void juego_division() {
	srand(time(NULL));


	int n1[8];
	int n2[8];

	for (int i = 0; i < 8; i++) {
		n1[i] = rand() % 3 + 2;
		int r = rand() % 3 + 1;
		n2[i] = n1[i] * r;
	}


	for (int i = 0; i < 8; i++) {
		ronda_division(n1[i], n2[i]);
	}
}