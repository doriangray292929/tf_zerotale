#pragma once
#include "mapas.h"
using namespace std;

void dibujarFlamin(int x, int y) {
	int dibujarFlamin[10][15] = { 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0,
								0, 3, 0, 0, 1, 1, 1, 4, 4, 4, 1, 1, 0, 1, 0,
								0, 3, 0, 1, 1, 4, 4, 4, 4, 4, 4, 4, 1, 4, 1,
								0, 3, 0, 1, 8, 8, 4, 4, 4, 4, 8, 8, 4, 4, 1,
								1, 3, 1, 4, 8, 0, 4, 4, 4, 4, 0, 8, 4, 1, 1,
								1, 3, 1, 4, 4, 4, 4, 0, 0, 4, 4, 4, 4, 4, 1,
								0, 3, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 0,
								0, 0, 0, 1, 1, 4, 4, 1, 1, 1, 4, 4, 1, 1, 0,
								0, 0, 2, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2,
								0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0 };

		char n219 = 219;
		char n178 = 178;
		char n177 = 177;
		char n176 = 176;
		char n174 = 174;
		char n247 = 247;

	for (int i = 0; i < 10; ++i) {

		for (int j = 0; j < 15; ++j) {

			if (dibujarFlamin[i][j] == 1) {

				color(DARK_RED);
				mapeo[i + y][j + x] = 1;
				gotoxy(j + x, i + y);
				cout << n219;
				clearColor();

			}
			if (dibujarFlamin[i][j] == 2) {

				color(DARK_WHITE);
				mapeo[i + y][j + x] = 1;
				gotoxy(j + x, i + y);
				cout << n176;
				clearColor();

			}
			if (dibujarFlamin[i][j] == 3) {

				color(DARK_WHITE);
				mapeo[i + y][j + x] = 1;
				gotoxy(j + x, i + y);
				cout << n219;
				clearColor();

			}
			if (dibujarFlamin[i][j] == 4) {

				color(DARK_YELLOW);
				mapeo[i + y][j + x] = 1;
				gotoxy(j + x, i + y);
				cout << n176;
				clearColor();

			}
			if (dibujarFlamin[i][j] == 8) {
				mapeo[i + y][j + x] = 1;
				
				gotoxy(j + x, i + y);
				cout << n219;

			}

		}

	}
}
