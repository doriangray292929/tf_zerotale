#pragma once
#include "tutor_suma.h"
#include "tutor_resta.h"
#include "tutor_multi.h"
#include "tutor_divi.h"
#include "upc.h"
#include "desafio_multi.h"

void bienvenidaDivin() {
    marcoTutorDivi();
    dibujarPinguino(15, 10);

    // Primer di�logo
    marco_dialogo(5, 45, 52, 9);
    gotoxy(55, 7); cout << "�Intruso en mis Gelidas Tierras!";
    _sleep(500);
    gotoxy(51, 8); cout << "Soy Divin, el temido Pinguino Rey.";
    _sleep(500);
    gotoxy(48, 9); cout << "Si deseas salvar tu aldea, debes demostrar";
    _sleep(500);
    gotoxy(49, 10); cout << "tu habilidad para dividir correctamente.";
    _sleep(500);
    gotoxy(50, 11); cout << "Alimenta equitativamente a mis secuaces.";
    _sleep(500);
    color(DARK_CYAN);
    gotoxy(58, 12); cout << "Que comience la prueba!";
    clearColor();

    limpiarPantallaZ();

    // Segundo di�logo
    marcoTutorDivi();
    dibujarPinguino(15, 10);
    marco_dialogo(5, 45, 52, 9);
    gotoxy(54, 8); cout << "Aqui esta tu desafio, joven Zero.";
    _sleep(500);
    gotoxy(47, 9); cout << "Te dare una cantidad de comida que debes";
    _sleep(500);
    gotoxy(47, 10); cout << "dividir igual entre mis pinguinos.";
    _sleep(500);
    color(DARK_CYAN);
    gotoxy(60, 11); cout << "Estas listo?";
    clearColor();
    _sleep(100);

    limpiarPantallaZ();

}
void bienvenidaSumin() {
    
        marcoTutorSuma();
        dibujar_sapito(15, 10);

        // Primer dialogo
        marco_dialogo(5, 38, 52, 9);
        gotoxy(48, 7); cout << "Intruso en mi Pradera Encantada!";
        _sleep(500);
        gotoxy(49, 8); cout << "Soy Sumin, la astuta Rana Guardiana.";
        _sleep(500);
        gotoxy(48, 9); cout << "Si deseas pasar, debes demostrar";
        _sleep(500);
        gotoxy(47, 10); cout << "tu habilidad para sumar correctamente.";
        _sleep(500);
        gotoxy(47, 11); cout << "Ayudandome a atrapar insectos.";
        _sleep(500);
        color(DARK_GREEN);
        gotoxy(55, 12); cout << "Que comience el reto!";
        clearColor();

        limpiarPantallaZ();

        // Segundo dialogo
        marcoTutorSuma();
        dibujar_sapito(15, 10);
        marco_dialogo(5, 38, 52, 9);
        gotoxy(52, 8); cout << "Aqui esta tu desafio, joven Zero.";
        _sleep(500);
        gotoxy(40, 9); cout << "Te dire cuantos sapos vendran a mi cena.";
        _sleep(500);
        gotoxy(45, 10); cout << "Atrapa un insecto para cada sapo.";
        _sleep(500);
        color(DARK_GREEN);
        gotoxy(60, 11); cout << "Estas listo?";
        clearColor();
        _sleep(100);

        limpiarPantallaZ();
    
}

void bienvenidaMultin() {

    marcoTutorMulti();
    mago(15, 10);

    // Primer dialogo
    marco_dialogo(5, 38, 52, 9);
    gotoxy(45, 7); cout << "Bienvenido a mi Bosque Magico!";
    _sleep(500);
    gotoxy(49, 8); cout << "Soy Multin, el Maestro Multiplicador.";
    _sleep(500);
    gotoxy(48, 9); cout << "Para avanzar, debes demostrar";
    _sleep(500);
    gotoxy(48, 10); cout << "tu habilidad para multiplicar.";
    _sleep(500);
    gotoxy(48, 11); cout << "Ordena mis cartas correctamente.";
    _sleep(500);
    color(DARK_MAGENTA);
    gotoxy(55, 12); cout << "Estas listo para el reto?";
    clearColor();

    limpiarPantallaZ();

    // Segundo dialogo
    marcoTutorMulti();
    mago(15, 10);
    marco_dialogo(5, 38, 52, 9);
    gotoxy(52, 8); cout << "Aqui esta tu desafio, joven Zero.";
    _sleep(500);
    gotoxy(40, 9); cout << "Te dare una multiplicacion que debes resolver.";
    _sleep(500);
    gotoxy(45, 10); cout << "Elige la carta con la respuesta correcta.";
    _sleep(500);
    color(DARK_MAGENTA);
    gotoxy(60, 11); cout << "Estas listo?";
    clearColor();
    _sleep(100);

    limpiarPantallaZ();

}
void bienvenidaRestin() {

    marcoTutorResta();
    dibujarFlamin(15, 10);

    // Primer dialogo
    marco_dialogo(5, 38, 52, 9);
    gotoxy(48, 7); cout << "Quien eres y porque estas aqui?";
    _sleep(500);
    gotoxy(49, 8); cout << "Soy Restin, el Rey de las Llamas.";
    _sleep(500);
    gotoxy(48, 9); cout << "Para avanzar, debes demostrar";
    _sleep(500);
    gotoxy(49, 10); cout << "tu habilidad para restar.";
    _sleep(500);
    gotoxy(42, 11); cout << "Enfrenta a mis secuaces flamitas y escoge bien.";
    _sleep(500);
    color(DARK_RED);
    gotoxy(55, 12); cout << "Que comience el duelo!";
    clearColor();

    limpiarPantallaZ();

    // Segundo dialogo
    marcoTutorResta();
    dibujarFlamin(15, 10);
    marco_dialogo(5, 38, 52, 9);
    gotoxy(52, 8); cout << "Aqui esta tu desafio, joven Zero.";
    _sleep(500);
    gotoxy(40, 9); cout << "Te enfrentaras a mis flamitas.";
    _sleep(500);
    gotoxy(45, 10); cout << "Resta para vencerlos y avanzar.";
    _sleep(500);
    color(DARK_RED);
    gotoxy(60, 11); cout << "Estas listo?";
    clearColor();
    _sleep(100);

    limpiarPantallaZ();

}
void despedidaRestin() {

    marcoTutorResta();
    dibujarFlamin(15, 10);

    // Primer dialogo
    marco_dialogo(5, 38, 52, 9);
    gotoxy(48, 7); cout << "Quien eres y porque estas aqui?";
    _sleep(500);
    gotoxy(49, 8); cout << "Soy Restin, el Rey de las Llamas.";
    _sleep(500);
    gotoxy(48, 9); cout << "Para avanzar, debes demostrar";
    _sleep(500);
    gotoxy(49, 10); cout << "tu habilidad para restar.";
    _sleep(500);
    gotoxy(42, 11); cout << "Enfrenta a mis secuaces flamitas y escoge bien.";
    _sleep(500);
    color(DARK_RED);
    gotoxy(55, 12); cout << "Que comience el duelo!";
    clearColor();

    limpiarPantallaZ();
}
void bienvenidaDragon() {
    // Primer dialogo
    dragon_dibujo(75, 2);
    marco_dialogo(5, 10, 52, 9);
    gotoxy(22, 7); cout << "Finalmente has llegado, joven Zero.";
    _sleep(500);
    gotoxy(20, 8); cout << "Soy Drakon, el Destructor de Aldeas.";
    _sleep(500);
    gotoxy(14, 9); cout << "En el pasado, arrase tu aldea, y ahora";
    _sleep(500);
    gotoxy(14, 10); cout << "vengo por esta. Solo tu puedes detenerme.";
    _sleep(500);
    gotoxy(14, 11); cout << "Demuestra tu poder matematico y salva a todos.";
    _sleep(500);
    color(DARK_RED);
    gotoxy(27, 12); cout << "Que comience el duelo final!";
    clearColor();

    limpiarPantallaZ();

    // Segundo dialogo - explicacion del juego
    dragon_dibujo(75, 2);
    marco_dialogo(5, 10, 63, 9);
    gotoxy(25, 8); cout << "Te enfrentaras a un juego de operaciones.";
    _sleep(500);
    gotoxy(15, 9); cout << "Te dare una operacion y debes recoger el numero correcto";
    _sleep(500);
    gotoxy(22, 10); cout << "moviendo con las teclas 'j' y 'l'.";
    _sleep(500);
    color(DARK_RED);
    gotoxy(27, 11); cout << "Estas listo para salvar la aldea?";
    clearColor();
    _sleep(100);

    limpiarPantallaZ();
}

void dibujarAnciano(int x, int y) {
    int anciano[10][15] = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0},
        {0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
        {0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
        {2, 2, 2, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0},
        {2, 2, 2, 2, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 0},
        {2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 0, 0},
        {2, 2, 2, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 0, 0},
        {0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0}
    };

    char n219 = 219; // Bloque s�lido
    char n178 = 178; // Bloque ligero
    char n177 = 177; // Bloque medio
    char n176 = 176; // Bloque suave

    for (int i = 0; i < 10; ++i) {
        for (int j = 0; j < 15; ++j) {
            gotoxy(x + j, y + i);
            switch (anciano[i][j]) {
            case 1:
                color(DARK_WHITE);
                cout << n219;
                clearColor();
                break;
            case 2:
                
                color(BRIGHT_BLACK);
                cout << n219;
                clearColor();
                break;
            case 3:
                color(DARK_YELLOW);
                cout << n219;
                clearColor();
                break;
            }
        }
    }
}
void bienvenidaEscuela() {

        // Dibuja al anciano en la posici�n especificada
        dibujarAnciano(55, 2);

        // Primer di�logo: Bienvenida a la escuela de tutores
        marco_dialogo(15, 30, 60, 9);
        gotoxy(43, 17); cout << "Hola, joven Zero!";
        _sleep(500);
        gotoxy(35, 18); cout << "Bienvenido a la escuela de tutores.";
        _sleep(500);
        gotoxy(34, 19); cout << "Aqui aprenderas matematicas.";
        _sleep(500);
        color(DARK_CYAN);
        gotoxy(38, 20); cout << "Una aventura epica te espera!";
        clearColor();

        _sleep(1000); // Esperar un momento antes de limpiar la pantalla
        limpiarPantallaZ();

        // Segundo di�logo: Instrucciones para la aventura
        dibujarAnciano(55, 2);
        marco_dialogo(15, 30, 60, 9);
        gotoxy(35, 17); cout << "En este colegio y en nuestro mundo,";
        _sleep(500);
        gotoxy(32, 18); cout << "para aprender matematicas y practicarlas";
        _sleep(500);
        gotoxy(32, 19); cout << "debes pisar las lozas rojas.";
        _sleep(500);
        gotoxy(32, 20); cout << "Se cuidadoso y valiente con tu aventura!";
        _sleep(500);
        color(DARK_CYAN);
        gotoxy(38, 21); cout << "Buena suerte, joven Zero!";
        clearColor();
        _sleep(1000);

        limpiarPantallaZ();

}
void despedidaAnciano() {
    // Dibuja al anciano en la posici�n especificada
    dibujarAnciano(55, 2);

    // Primer di�logo: Felicitaciones por vencer al drag�n
    marco_dialogo(15, 30, 60, 9);
    gotoxy(46, 17); cout << "Lo has logrado, joven Zero!";
    _sleep(500);
    gotoxy(40, 18); cout << "Has demostrado tu valia y derrotado a Drakon.";
    _sleep(500);
    gotoxy(40, 19); cout << "Tu habilidad matematica ha salvado la aldea.";
    _sleep(500);
    color(DARK_CYAN);
    gotoxy(50, 20); cout << "Eres un verdadero heroe!";
    clearColor();

    _sleep(1000); // Esperar un momento antes de limpiar la pantalla
    limpiarPantallaZ();

    // Segundo di�logo: Reflexi�n sobre la importancia de las matem�ticas
    dibujarAnciano(55, 2);
    marco_dialogo(15, 30, 60, 9);
    gotoxy(39, 17); cout << "Recuerda, Zero, las matematicas no solo te han";
    _sleep(500);
    gotoxy(38, 18); cout << "ayudado a salvar la aldea, sino que tambien";
    _sleep(500);
    gotoxy(38, 19); cout << "son una herramienta poderosa en la vida diaria.";
    _sleep(500);
    gotoxy(38, 20); cout << "Continua aprendiendo y utilizando este poder!";
    _sleep(500);
    color(DARK_CYAN);
    gotoxy(43, 21); cout << "Las matematicas son la clave del exito!";
    clearColor();
    _sleep(1000);

    limpiarPantallaZ();
}


