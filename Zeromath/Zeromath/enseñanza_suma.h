#pragma once
#include "upc.h"
#include "utilidades.h"



void dibujar_mosca(int x, int y, int d) {
	int moscad[10][20] = {
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,
0,10,10,10,10,10,0,1,0,1,0,0,0,0,0,0,0,0,0,0,
10,10,0,10,0,10,10,10,10,10,0,0,0,0,0,0,0,0,0,0,
0,10,10,10,10,10,10,10,10,10,0,0,0,0,0,0,0,0,0,0,
0,1,0,0,1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	};

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 10; ++i) {

		for (int j = 0; j < 20; ++j) {

			if (moscad[i][j] == 10) {

				color(DARK_RED);
				mapeo[y + i][x + j] = 1;
				gotoxy(x + j, y + i);
				if (d == 1) {
					cout << n219;
				}
				else {
					cout << ' ';
				}
				clearColor();

			}
			if (moscad[i][j] == 1) {

				color(DARK_WHITE);
				mapeo[y + i][x + j] = 1;
				gotoxy(x + j, y + i);
				if (d == 1) {
					cout << n219;
				}
				else {
					cout << ' ';
				}
				clearColor();

			}
		}

	}

}




void dibujarfila_res(int n, int x, int y) {

	for (int i = 0; i < n; i++) {
		dibujar_mosca(x, y, 1);
		x += 14;
	}
	
	
}
void marco_res_moscas(int res) {
	system("cls");
	marco_dialogo(0, 20, 80, 5);
	gotoxy(40, 2);
	cout << "En total debo llevar " << res << " insectos a la cena";
	dibujarfila_res(res, 1, 10);
}

void dibujar_lengua(int n, int x, int y) {
	char nc = 219;
	for (int i = 0; i < n; i++) {
		gotoxy(x, y + i);
		color(BRIGHT_MAGENTA);
		cout << nc;
		clearColor();
		gotoxy(x + 1, y + i);
		color(BRIGHT_MAGENTA);
		cout << nc;
		clearColor();
	}

	Sleep(50);
	dibujar_mosca(53, 4, 0);
	for (int i = 0; i < n; i++) {
		gotoxy(x, y + i);
		color(BRIGHT_MAGENTA);
		cout << ' ';
		clearColor();
		gotoxy(x + 1, y + i);
		color(BRIGHT_MAGENTA);
		cout << ' ';
		clearColor();
	}
	Sleep(100);
	dibujar_mosca(53, 4, 1);
}

void dibujarfmo(int n, int x, int y, int d) {
	char nc = 219;
	int ix = x;
	for (int i = 0; i < n; i++) {
		if (i == 6 || i == 12) {
			y += 2;
			x -= 12;
		}
		
		gotoxy(x + i*2, y);
		color(DARK_RED);
		if (d == 1) {
			cout << nc;
		}
		else {
			cout << ' ';
		}
	}

}

void dibujar_saco(int x, int y) {
	int saco[10][30] = {
0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,
0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,
0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,
0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,
0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,
0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,
0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,
0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,
0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0
	};
	char n219 = 219;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 30; j++) {

			if (saco[i][j] == 1) {

				color(BRIGHT_RED);
				mapeo[y + i][x + j] = 1;
				gotoxy(x + j, y + i);
					cout << n219;
			
				clearColor();

			}
		}
	}
}

void borrarfmo(int x, int y) {
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 13; j++) {
			gotoxy(x + j, y + i);
			cout << ' ';
		}
	}
}
bool opciones(int& n) {

	for (int i = 0; i < 25; i++)
	{
		gotoxy(8 + i, 11);
		cout << " ";
	}
	gotoxy(8, 11);
	cout << "Contador de insectos: " << n;
	char tecla = _getch();
	

		switch (tecla) {
		case 'z':
			if (n < 15) {
				n++;
				dibujarfmo(n, 13, 15, 1);
				dibujar_lengua(11, 58, 10);
				
			}
			break;
		case 'x':
			if (n > 0) {
				n--;
				borrarfmo(13, 15);
				dibujarfmo(n, 13, 15, 1);
			}
			break;
		case 'c':
			return true;
			break;
		}
	
		return false;
}

void instrucciones(int x, int y) {
	gotoxy(x, y);
	cout << "Presiona --> Z <-- para atrapar insectos";
	gotoxy(x, y + 5);
	cout << "Presiona --> X <-- para soltar insectos";
	gotoxy(x, y + 10);
	cout << "Presiona --> C <-- para terminar";
}

void ronda_suma(int s1, int s2) {
	int res_p = 0;
	int res = s1 + s2;
	
	marco_dialogo(0, 20, 80, 5);
	gotoxy(25, 2);
	if (s1 > 1 && s2 > 1) {
		cout << "Hay " << s1 << " sapos en mi casa. Cuantos insectos llevas si " << s2 << " mas vienen a cenar?";
	}
	else if (s1 > 1 && s2 == 1) {
		cout << "Hay " << s1 << " sapos en mi casa. Cuantos insectos llevas si " << s2 << " mas viene a cenar?";
	}
	else if (s1 == 1 && s2 > 1) {
		cout << "Hay " << s1 << " sapo en mi casa. Cuantos insectos llevas si " << s2 << " mas vienen a cenar?";
	}
	else if (s1 == 1 && s2 == 1) {
		cout << "Hay " << s1 << " sapo en mi casa. Cuantos insectos llevas si " << s2 << " mas viene a cenar?";
	}
	dibujar_saco(4, 13);
	dibujar_sapito(50, 20);
	dibujar_mosca(53, 4, 1);
	instrucciones(80, 10);
	bool continuar = true;
	while (continuar) {
		bool ver = opciones(res_p);
		if (ver) { continuar = false; };
		
		
	}
	system("cls");
	verificar_res(res_p, res);
	sleep4(1000);
	marco_res_moscas(res);
	dibujarfila_res(res, 1, 10);
	sleep4(1500);
	system("cls");
}

void juego_suma() {
	srand(time(NULL));
	

	int n1[8];
	int n2[8];
	for (int i = 0; i < 8; i++) {
		n1[i] = rand() % 8 + 1;
		n2[i] = rand() % 8 + 1;
		if (n1[i] + n2[i] > 8) {
			while (n1[i] + n2[i] > 8) {
				n1[i] = rand() % 8 + 1;
				n2[i] = rand() % 8 + 1;
			}
		}
	}

	for (int i = 0; i < 8; i++) {
		ronda_suma(n1[i], n2[i]);
	}
}
