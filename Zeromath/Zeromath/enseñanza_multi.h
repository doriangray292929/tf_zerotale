#pragma once
#include <iostream>
#include "upc.h"
#include "utilidades.h"
#include "sapito.h"
#include "numeros.h"

using namespace System;
using namespace std;
//Posicion cartas
int xc = 35;
int yc = 10;

void dibujar_cAtras(int x, int y) {
    int c_array[7][10] = {
                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                        1, 1, 11, 1, 11, 11, 1, 11, 1, 1,
                        1, 1, 1, 11, 11, 11, 11, 1, 1, 1,
                        1, 1, 11, 11, 11, 11, 11, 11, 1, 1,
                        1, 1, 1, 11, 11, 11, 11, 1, 1, 1,
                        1, 1, 11, 1, 11, 11, 1, 11, 1, 1,
                        1, 1, 1, 1, 1, 1, 1, 1, 1, 1
                            };
    char n219 = 219;
    for (int i = 0; i < 7; ++i) {

        for (int j = 0; j < 10; ++j) {

            if (c_array[i][j] == 1) {
                color(BRIGHT_CYAN);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (c_array[i][j] == 11) {
                color(BRIGHT_YELLOW);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();
            }
        }

    }
}

void dibujar_cDelante(int x, int y, int op) {
    int c_arrays[7][10] = {
1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
1, 2, 2, 0, 0, 0, 0, 2, 2, 1,
1, 2, 0, 0, 0, 0, 0, 0, 2, 1,
1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
1, 2, 0, 0, 0, 0, 0, 0, 2, 1,
1, 2, 2, 0, 0, 0, 0, 2, 2, 1,
1, 1, 1, 1, 1, 1, 1, 1, 1, 1
    };
    char n219 = 219;
    char n178 = 178;
    for (int i = 0; i < 7; ++i) {

        for (int j = 0; j < 10; ++j) {

            if (c_arrays[i][j] == 1) {
                color(BRIGHT_CYAN);
                gotoxy(x + j, y + i);
                cout << n219;
                clearColor();

            }
            if (c_arrays[i][j] == 2) {
                color(DARK_RED);
                gotoxy(x + j, y + i);
                cout << n178;
                clearColor();
            }
        }

        gotoxy(x + 4, y + 3);
        cout << op;

    }
}
void opciones_cAtras(int colorn) {
    int xi = xc, yi = yc;
    int temp = xi;
    coloresOpciones(colorn);
    dibujar_cAtras(xi, yi);
    clearColor();
    xi += 13;
    dibujar_cAtras(xi, yi);
    xi += 13;
    dibujar_cAtras(xi, yi);
    xi += 13;
    dibujar_cAtras(xi, yi);

    xi = temp;
    yi += 10;
    dibujar_cAtras(xi, yi);
    xi += 13;
    dibujar_cAtras(xi, yi);
    xi += 13;
    dibujar_cAtras(xi, yi);
    xi += 13;
    dibujar_cAtras(xi, yi);


}



bool elegir_opcionCarta = true;
bool respuestaIncorrectaCarta = false;

void elegir_c(char opcionCorrecta, string opcion1, string opcion2, string opcion3, int colorn) {

    int xopcion = 15;
    int yopcion = 17;

    char opcionActual = '1';  // Opcin inicial

    // Pocisin de las opciones X

    int opciones[3] = { 17, 45, 68 };

    // Dibujar 'v' en la posicin inicial
    gotoxy(opciones[0], yopcion + 1);
    cout << 'v';

    while (elegir_opcion) {
        if (_kbhit()) {  // Verificar si se ha presionado una tecla
            char tecla = _getch();
            respuestaIncorrectaCarta = false;


            // Limpiar 'v' en la posicin actual
            gotoxy(opciones[opcionActual - '1'], yopcion + 1);
            cout << " ";

            gotoxy(opciones[opcionActual - '1'], yopcion);

            coloresOpciones(colorn);
            marco_dialogo2(9, 20, 17, 5, opcion1);
            clearColor();
            marco_dialogo2(36, 20, 17, 5, opcion2);
            marco_dialogo2(63, 20, 17, 5, opcion3);

            if (tecla == 'l') {  // Mover a la derecha
                if (opcionActual < '3') {
                    opcionActual++;
                }
            }
            else if (tecla == 'j') {  // Mover a la izquierda
                if (opcionActual > '1') {
                    opcionActual--;
                }
            }
            else if (tecla == 'z') {  // Salir del bucle
                if (opcionActual == opcionCorrecta) {
                    verificar_res(2, 2);
                    elegir_opcionCarta = false;
                    limpiarPantallaZ();
                    break;
                }
                else {
                    verificar_res(2, 1);

                    elegir_opcionCarta = false;
                    respuestaIncorrectaCarta = true;
                    limpiarPantallaZ();
                    break;
                }
            }

            if (opcionActual == '1') coloresOpciones(colorn);  marco_dialogo2(9, 20, 17, 5, opcion1); clearColor();
            if (opcionActual == '2') coloresOpciones(colorn); marco_dialogo2(36, 20, 17, 5, opcion2); clearColor();
            if (opcionActual == '3') coloresOpciones(colorn);; marco_dialogo2(63, 20, 17, 5, opcion3); clearColor();

            gotoxy(opciones[opcionActual - '1'], yopcion + 1);
            cout << 'v';

        }
    }

}

bool elegir_opcionC = true;
bool respuestaCorrectaC = false;


void elegir6(char opcionCorrecta, string opcion1, string opcion2, string opcion3, string opcion4, string opcion5, string opcion6, int colorn) {
    int xopcion = 15;
    int yopcion = 6;

    char opcionActual = '1';  // Opci�n inicial

    // Posiciones de las opciones X y Y
    int opcionesX[3] = { 25, 53, 82 };
    int opcionesY[2] = { 11, 18 };

    // Dibujar 'v' en la posici�n inicial
    gotoxy(opcionesX[0], opcionesY[0] - 2);
    cout << 'v';

    // Dibujar opciones iniciales
    coloresOpciones(colorn);
    marco_dialogo2(17, 6 + 5, 17, 5, opcion1);
    marco_dialogo2(45, 6 + 5, 17, 5, opcion2);
    marco_dialogo2(72, 6 + 5, 17, 5, opcion3);
    marco_dialogo2(17, 13 + 5, 17, 5, opcion4);
    marco_dialogo2(45, 13 + 5, 17, 5, opcion5);
    marco_dialogo2(72, 13 + 5, 17, 5, opcion6);
    clearColor();

    while (elegir_opcionC) {
        if (_kbhit()) {  // Verificar si se ha presionado una tecla
            char tecla = _getch();
            respuestaCorrectaC = false;

            // Limpiar 'v' en la posici�n actual
            int posX = opcionesX[(opcionActual - '1') % 3];
            int posY = opcionesY[(opcionActual - '1') / 3];
            gotoxy(posX, posY - 2);
            cout << " ";

            if (tecla == 'l') {  // Mover a la derecha
                if (opcionActual % 3 != '0') {
                    opcionActual++;
                }
            }
            else if (tecla == 'j') {  // Mover a la izquierda
                if (opcionActual % 3 != '1') {
                    opcionActual--;
                }
            }
            else if (tecla == 'i') {  // Mover hacia arriba
                if (opcionActual > '3') {
                    opcionActual -= 3;
                }
            }
            else if (tecla == 'k') {  // Mover hacia abajo
                if (opcionActual <= '3') {
                    opcionActual += 3;
                }
            }
            else if (tecla == 'z') {  // Seleccionar opci�n
                if (opcionActual == opcionCorrecta) {
                    verificar_res(2, 2);
                    elegir_opcionC = false;
                    respuestaCorrectaC = true;
                    sleep4(1000);
                    break;
                }
                else {
                    verificar_res(2, 1);
                    elegir_opcionC = false;
                    respuestaCorrectaC = false;
                    sleep4(1000);
                    break;
                }
            }

            // Asegurar que la flecha 'v' no se mueva fuera de las 6 opciones
            if (opcionActual < '1') opcionActual = '1';
            if (opcionActual > '6') opcionActual = '6';

            // Dibujar 'v' en la nueva posici�n
            posX = opcionesX[(opcionActual - '1') % 3];
            posY = opcionesY[(opcionActual - '1') / 3];
            gotoxy(posX, posY - 2);
            cout << 'v';
        }
    }
}

void juego_multiplicacion() {
    int n1, n2;
    string opt[8][6] = {
        // 2
        {"1","25","20","9","16","14"},
        // 6
        {"8","19","31","40","1","14"},
        // 4
        {"4","3","15","12","6","8"},
        // 5 
         {"7","3","13","15","16","2"},
         // 1
         {"27","4","2","19","15","7"},
         // 3
         {"2","49","36","1","2","5"},
         // 6
         {"4","35","1","29","24","40"},
         // 2
         {"3","10","1","23","4","35"}

    };
    int num12[8][2] = {
        {5, 5},
        {7, 2},
        {3, 4},
        {2, 8},
        {3, 9},
        {6, 6},
        {8, 5},
        {5, 2}
    };

    char res[8] = {
        '2', '6', '4', '5', '1', '3', '6', '2'
    };
    for (int i = 0; i < 8; i++) {
        gotoxy(50, 2);
        cout << "Cuanto es " << num12[i][0] << " x " << num12[i][1] << '?';
        marco_dialogo(0, 20, 80, 5);
        elegir6(res[i], opt[i][0], opt[i][1], opt[i][2], opt[i][3], opt[i][4], opt[i][5], 1);
        elegir_opcionC = true;
        respuestaCorrectaC = false;
        system("cls");
        marco_dialogo(0, 20, 80, 5);
        gotoxy(50, 2);
      
        cout << "Correcto! " << num12[i][0] << " x " << num12[i][1] << " es " << num12[i][0] * num12[i][1];
       
        dibujar_cDelante(40, 10, num12[i][0]);
        dibujar_cDelante(55, 10, num12[i][1]);
        dibujar_cDelante(70, 10, num12[i][0] * num12[i][1]);
        sleep4(2000);
        system("cls");
    }
}