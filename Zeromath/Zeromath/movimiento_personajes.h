#pragma once
#include <iostream>
#include <conio.h>
#include <cmath>
#include "upc.h"
#include "mapas.h"
#include "estados.h"

using namespace std;
using namespace System;

extern int cmtotal;
extern bool boss_final;

extern int mapeo[30][120];
extern int mapa_actual;

int zerox = 59;
int zeroy = 24;

extern bool e_movimiento;
extern bool e_mapa;

struct MatrizPersonaje {
    int matriz[3][6];
};

struct Personaje {
    int x;
    int y;
    char n219 = 219;
    MatrizPersonaje zero;
    MatrizPersonaje zeroJ;
    MatrizPersonaje zeroL;
    MatrizPersonaje zeroK;

    Personaje() {
        zero = { {
            {0, 1, 1, 1, 1, 0},
            {1, 0, 1, 1, 0, 1},
            {1, 1, 1, 1, 1, 1}
        } };

        zeroJ = { {
            {0, 1, 1, 1, 1, 0},
            {1, 0, 1, 0, 1, 1},
            {1, 1, 1, 1, 1, 1}
        } };

        zeroL = { {
            {0, 1, 1, 1, 1, 0},
            {1, 1, 0, 1, 0, 1},
            {1, 1, 1, 1, 1, 1}
        } };

        zeroK = { {
            {0, 1, 1, 1, 1, 0},
            {1, 0, 1, 1, 0, 1},
            {1, 1, 1, 1, 1, 1}
        } };
    }

    bool colision(int nx, int ny, MatrizPersonaje& caract) {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 6; ++j) {
                if (caract.matriz[i][j] == 1 && mapeo[ny + i][nx + j] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    bool colisionLose(int nx, int ny, MatrizPersonaje& caract) {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 6; ++j) {
                if (mapeo[ny + i][nx + j] == 40) {
                    return true;
                }
            }
        }
        return false;
    }
    bool colisionObjetoBorrar(int nx, int ny, MatrizPersonaje& caract, int& idVitamina) {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 6; ++j) {
                if (caract.matriz[i][j] == 1 && mapeo[ny + i][nx + j] >= 700 && mapeo[ny + i][nx + j] <= 799) {
                    idVitamina = mapeo[ny + i][nx + j];
                    return true;
                }
            }
        }
        return false;
    }


    void verificarCambioMapa(int& nx, int& ny, MatrizPersonaje& caract) {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 6; ++j) {
                if (caract.matriz[i][j] == 1) {
                    switch (mapeo[ny + i][nx + j]) {
                    case 22:
                        mapa_actual += 5;
                        ny = 27;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 24:
                        mapa_actual -= 5;
                        ny = 3;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 23:
                        mapa_actual += 2;
                        nx = 3;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 21:
                        mapa_actual -= 2;
                        nx = 110;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 25:
                        mapa_actual = 20;
                        ny = 27;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 26:
                        mapa_actual = 10;
                        ny = 20;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 100:
                        mapa_actual = 100;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 101:
                        mapa_actual = 101;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 102:
                        mapa_actual = 102;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 103:
                        mapa_actual = 103;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 80:
                        mapa_actual = 80;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 81:
                        mapa_actual = 81;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 82:
                        mapa_actual = 82;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    case 83:
                        mapa_actual = 83;
                        e_movimiento = false;
                        e_mapa = true;
                        return;
                    }
                }
            }
        }
        if (cmtotal == 4) {
            e_movimiento = false;
            boss_final = true;
        }
    }

    void borrar(int pzerox, int pzeroy, MatrizPersonaje& caract) {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 6; ++j) {
                if (caract.matriz[i][j] == 1 || caract.matriz[i][j] == 0) {
                    gotoxy(pzerox + j, pzeroy + i);
                    cout << " ";
                    mapeo[pzeroy + i][pzerox + j] = 0;
                }
            }
        }
    }

    void dibujar(int pzerox, int pzeroy, MatrizPersonaje& caract) {
        char n219 = 219;
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 6; ++j) {
                if (caract.matriz[i][j] == 1) {
                    color(BRIGHT_MAGENTA);
                    gotoxy(pzerox + j, pzeroy + i);
                    cout << n219;
                    clearColor();
                }
            }
        }
    }

    void mover(char direccion) {
        int nuevoX = x;
        int nuevoY = y;
        MatrizPersonaje* caract;

        if (direccion == 'i') {
            nuevoY--;
            caract = &zero;
        }
        else if (direccion == 'k') {
            nuevoY++;
            caract = &zeroK;
        }
        else if (direccion == 'j') {
            nuevoX--;
            caract = &zeroJ;
        }
        else if (direccion == 'l') {
            nuevoX++;
            caract = &zeroL;
        }
        else {
            return;
        }

        if (nuevoX >= 0 && nuevoX < 120 && nuevoY >= 0 && nuevoY < 30 && !colision(nuevoX, nuevoY, *caract)) {
            borrar(x, y, *caract);
            x = nuevoX;
            y = nuevoY;
            verificarCambioMapa(x, y, *caract);
            dibujar(x, y, *caract);
        }

    }
};
Personaje personaje;

void zeromov(int& x, int& y, int mapeo[30][120]) {
    Personaje personaje;
    personaje.x = x;
    personaje.y = y;
    personaje.dibujar(personaje.x, personaje.y, personaje.zero);

    while (true) {
        if (_kbhit()) {
            char tecla = _getch();
            if (tecla == 'i' || tecla == 'k' || tecla == 'j' || tecla == 'l') {

                personaje.mover(tecla);
                if (e_mapa) {
                    x = personaje.x;
                    y = personaje.y;
                    break;
                }
            }
        }
    }
}

void zeromovf(int& x, int& y, int mapeo[30][120]) {

    if (_kbhit()) {
        char tecla = _getch();

        if (tecla == 'i' || tecla == 'k' || tecla == 'j' || tecla == 'l') {
            personaje.mover(tecla);
            if (e_mapa) {
                x = personaje.x;
                y = personaje.y;
            }
        }
    }
}

