#pragma once
#include "mapas.h"

using namespace std;

void dibujar_uno(int x, int y) {

	int uno[4][7] = {
		0, 1, 1, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0,
		0, 1, 1, 1, 1, 1, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 7; ++j) {

			if (uno[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}

void dibujar_dos(int x, int y) {

	int dos[4][7] = {
		0, 1, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 1, 0,
		0, 0, 1, 1, 0, 0, 0,
		0, 1, 1, 1, 1, 1, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 7; ++j) {

			if (dos[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_tres(int x, int y) {

	int tres[5][7] = {
		0, 1, 1, 1, 1, 0, 0, 
		0, 0, 0, 0, 1, 0, 0, 
		0, 0, 1, 1, 1, 0, 0, 
		0, 0, 0, 0, 1, 0, 0, 
		0, 1, 1, 1, 1, 0, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 7; ++j) {

			if (tres[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_cuatro(int x, int y) {

	int cuatro[5][6] = {0, 1, 0, 0, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0,
						0, 0, 0, 0, 1, 0,
						0, 0, 0, 0, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (cuatro[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_cinco(int x, int y) {

	int cinco[5][6] = {
						0, 1, 1, 1, 0, 0,
						0, 1, 0, 0, 0, 0, 
						0, 1, 1, 1, 0, 0, 
						0, 0, 0, 1, 0, 0, 
						0, 1, 1, 1, 0, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (cinco[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}

void dibujar_seis(int x, int y) {

	int seis[5][6] = {
						0, 1, 1, 1, 1, 0, 
						0, 1, 0, 0, 0, 0, 
						0, 1, 1, 1, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (seis[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}

void dibujar_siete(int x, int y) {

	int siete[5][6] = {
						0, 1, 1, 1, 0, 0, 
						0, 0, 0, 1, 0, 0, 
						0, 0, 1, 1, 1, 0, 
						0, 0, 0, 1, 0, 0, 
						0, 0, 0, 1, 0, 0};

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (siete[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_ocho(int x, int y) {

	int ocho[5][6] = {
						0, 1, 1, 1, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 1, 1, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (ocho[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_nueve(int x, int y) {

	int nueve[5][6] = {
						0, 1, 1, 1, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 1, 1, 1, 0, 
						0, 0, 0, 0, 1, 0, 
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (nueve[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_cero(int x, int y) {

	int cero[5][6] = {
						0, 1, 1, 1, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 0, 0, 1, 0, 
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;

	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (cero[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_simbMas(int x, int y) {

	int suma[3][3] = {	0, 1, 0, 
						1, 1, 1, 
						0, 1, 0  };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (suma[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_estrellaPequeña(int x, int y) {

	int suma[3][3] = { 0, 1, 0,
						1, 1, 1,
						0, 1, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {
			
			if (suma[i][j] == 1) {
				color(DARK_YELLOW);
				gotoxy(x + j, y + i);
				cout << n219;
				clearColor();
			}
		}

	}
}
void dibujar_simbPor(int x, int y) {

	int suma[3][3] = { 1, 0, 1,
						0, 1, 0,
						1, 0 , 1 };

	char n219 = 219;


	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (suma[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}

void dibujar_simbIgual(int x, int y) {

	int igual[3][3] = { 1, 1, 1,
						0, 0, 0,
						1, 1, 1 };

	char n219 = 219;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (igual[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_simbResta(int x, int y) {

	int igual[3][3] = { 0, 0, 0,
						1, 1, 1,
						0, 0, 0 };

	char n219 = 219;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (igual[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void dibujar_incog(int x, int y) {

	int incog[5][4] = { 1, 1, 1, 1, 
						0, 0, 1, 1, 
						0, 1, 1, 0, 
						0, 0, 0, 0, 
						0, 1, 1, 0 };

	char n219 = 219;

	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 4; ++j) {

			if (incog[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}

void borrar_uno(int x, int y) {

	int uno[4][7] = {
		0, 1, 1, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0,
		0, 0, 0, 1, 0, 0, 0,
		0, 1, 1, 1, 1, 1, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 7; ++j) {

			if (uno[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}

void borrar_dos(int x, int y) {

	int dos[4][7] = {
		0, 1, 1, 1, 1, 0, 0,
		1, 0, 0, 0, 1, 1, 0,
		0, 0, 1, 1, 0, 0, 0,
		0, 1, 1, 1, 1, 1, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 7; ++j) {

			if (dos[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_tres(int x, int y) {

	int tres[5][7] = {
		0, 1, 1, 1, 1, 0, 0,
		0, 0, 0, 0, 1, 0, 0,
		0, 0, 1, 1, 1, 0, 0,
		0, 0, 0, 0, 1, 0, 0,
		0, 1, 1, 1, 1, 0, 0 };

	char n219 = 219;
	char n178 = 178;
	char n177 = 177;
	char n176 = 176;

	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 7; ++j) {

			if (tres[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_cuatro(int x, int y) {

	int cuatro[5][6] = { 0, 1, 0, 0, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0,
						0, 0, 0, 0, 1, 0,
						0, 0, 0, 0, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (cuatro[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_cinco(int x, int y) {

	int cinco[5][6] = {
						0, 1, 1, 1, 0, 0,
						0, 1, 0, 0, 0, 0,
						0, 1, 1, 1, 0, 0,
						0, 0, 0, 1, 0, 0,
						0, 1, 1, 1, 0, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (cinco[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}

void borrar_seis(int x, int y) {

	int seis[5][6] = {
						0, 1, 1, 1, 1, 0,
						0, 1, 0, 0, 0, 0,
						0, 1, 1, 1, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (seis[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}

void borrar_siete(int x, int y) {

	int siete[5][6] = {
						0, 1, 1, 1, 0, 0,
						0, 0, 0, 1, 0, 0,
						0, 0, 1, 1, 1, 0,
						0, 0, 0, 1, 0, 0,
						0, 0, 0, 1, 0, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (siete[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_ocho(int x, int y) {

	int ocho[5][6] = {
						0, 1, 1, 1, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (ocho[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_nueve(int x, int y) {

	int nueve[5][6] = {
						0, 1, 1, 1, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0,
						0, 0, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;


	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (nueve[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_cero(int x, int y) {

	int cero[5][6] = {
						0, 1, 1, 1, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 0, 0, 1, 0,
						0, 1, 1, 1, 1, 0 };

	char n219 = 219;

	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 6; ++j) {

			if (cero[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_simbMas(int x, int y) {

	int tres[3][3] = { 0, 1, 0,
						1, 1, 1,
						0, 1, 0 };


	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (tres[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_simbResta(int x, int y) {

	int igual[3][3] = { 0, 0, 0,
						1, 1, 1,
						0, 0, 0 };

	char n219 = 219;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (igual[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_incog(int x, int y) {

	int incog[5][4] = { 1, 1, 1, 1,
						0, 0, 1, 1,
						0, 1, 1, 0,
						0, 0, 0, 0,
						0, 1, 1, 0 };

	char n219 = 219;

	for (int i = 0; i < 5; ++i) {

		for (int j = 0; j < 4; ++j) {

			if (incog[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_simbPor(int x, int y) {

	int suma[3][3] = { 1, 0, 1,
						0, 1, 0,
						1, 0 , 1 };

	char n219 = 219;


	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (suma[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << " ";

			}
		}

	}
}
void dibujar_simbDiv(int x, int y) {

	int tres[4][4] = { 0, 0, 0, 1,
					   0, 0, 1, 0,
					   0, 1, 0, 0,
					   1, 0, 0, 0 };
	char n219 = 219;

	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 4; ++j) {

			if (tres[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << n219;

			}
		}

	}
}
void borrar_simbDiv(int x, int y) {

	int tres[4][4] = { 1, 0, 0, 1,
					   0, 0, 1, 0,
					   0, 1, 0, 0,
					   1, 0, 0, 1 };


	for (int i = 0; i < 4; ++i) {

		for (int j = 0; j < 4; ++j) {

			if (tres[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}
void borrar_simbIgual(int x, int y) {

	int igual[3][3] = { 1, 1, 1,
						0, 0, 0,
						1, 1, 1 };


	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			if (igual[i][j] == 1) {
				gotoxy(x + j, y + i);
				cout << ' ';

			}
		}

	}
}